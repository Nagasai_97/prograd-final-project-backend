const jwt = require('jsonwebtoken');
function auth(req,res,next) {
  let accessToken = req.headers.accesstoken
  let refreshToken = req.headers.refreshtoken
  if(accessToken !== 'undefined'){
            jwt.verify(accessToken,process.env.SECRET_ACCESS_TOKEN,(error,user)=>{
              if(!error){
                  req.user=user;
                  next();
                }
                else {
                  jwt.verify(refreshToken,process.env.SECRET_REFRESH_TOKEN,(error,user)=>{
                    if(!error){
                      let newAcessToken = jwt.sign({
                        id:user._id,
                        username:user.email
                      },process.env.SECRET_ACCESS_TOKEN,{expiresIn:'30s'})
                     res.cookie('accessToken',newAcessToken)
                      next();
                    }else {
                      return res.status(500).json({status:'error',error:"refresh token error"})
                    }

                  })        
                }
            })
  }
  else {
    return res.status(500).json({status:'error',error:"access token error"})
  }
}

module.exports = auth;