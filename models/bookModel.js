const mongoose = require("mongoose");
const BookcSchema = new mongoose.Schema({

    bookname: {
        type: String,
        required: true,
       
},
    bookImg:{
        type: String,
        required: true,
},
    pdfLink: {
        type: String,
        required: true,
        },
    type: {
        type: String,
        required: true,
},
    status:{
        type: String,
        required: true,
},
    userid :{
        type: String,
        required: true,
},
    
  
})

const Book = mongoose.model("Bookchema", BookcSchema);

module.exports = Book;