const mongoose = require("mongoose");

const RegisterSchema = new mongoose.Schema({
        
        email: {
                type: String,
                required: true,
                unique:true
        },
        firstname: {
                type: String,
                required: true,
               
        },
        lastname: {
                type: String,
                required: true,
        },
        password: {
                type: String,
                required: true,
        },   

})

const Register = mongoose.model("register", RegisterSchema);

module.exports = Register;
