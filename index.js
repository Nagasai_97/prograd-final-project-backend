require('dotenv').config();
const express = require('express')
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser')
const app = express();
app.use(express.json()) 
app.use(require('cors')());
app.use(cookieParser())
global.TextEncoder = require("util").TextEncoder;

app.use('/login',require('./routes/loginroute'))
app.use('/signup',require('./routes/registerroute'))
app.use('/addbook',require('./routes/addBookRoute'))
app.use('/allbooks',require('./routes/allBooks'))
app.use('/readbook',require('./routes/readSignglebookRoute'))
app.use('/updateBook',require('./routes/updateBookRoute'))
app.use('/deleteBook',require('./routes/deleteBookRoute'))
app.use('/allrequests',require('./routes/allRequestsRoute'))
app.use('/mybooks',require('./routes/myBooksroute'))
app.use('/getProfile',require('./routes/profiledetailsRoute'))
app.use('/updateprofile',require('./routes/updateProfileRoute'))
app.use('/filter',require('./routes/filetrRoute'))
app.use('/landingpage',require('./routes/landingPageRoute'))
app.use('/upload',require('./routes/fileUploadRoute'))
app.use('/filterByUser',require('./routes/filetrByUserRoute'))
app.use('/search',require('./routes/searchRoute'))

mongoose.connect(process.env.DATABASE_CONNECTION).
 then(()=>{
     console.log('database connected sucefully')
 }).catch(err=>{
     console.log('error',err)
 })

const PORT = process.env.PORT || 4000;

app.listen(PORT,()=>console.log(`welcome to node ${PORT}`))