const router = require('express').Router();
const BookModel  = require('../models/bookModel');
const auth = require('../midleware/auth')

router.put('/:id',auth,async(req,res)=>{
    const id = req.params.id
    const data = req.body
    const updateBook = await BookModel.findByIdAndUpdate({_id:id},data,{new: true});
    try{
      res.send(updateBook)
    } catch(error) {
      res.status(500).send(error);
    }

})

module.exports = router;