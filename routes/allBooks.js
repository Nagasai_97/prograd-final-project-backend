const router = require('express').Router();
const BookModel = require('../models/bookModel')
const auth = require('../midleware/auth')

router.get('',auth,async(req,res) => {
    const books = await BookModel.find({}).where('status').equals('active');
    try {
      res.json(books);
    } catch (error) {
      res.status(500).send(error);
    }
})

module.exports = router;