const router = require('express').Router();
const BookModel = require('../models/bookModel')
const auth = require('../midleware/auth')
router.get('/:id',auth,async(req,res) => {
    const id = req.params.id
    const myBooks = await BookModel.find({userid:id}).where('status').equals('active');
    try{
      res.send(myBooks)
    } catch(error) {
      res.status(500).send(error);
    }
})

module.exports = router;