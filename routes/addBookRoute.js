const router = require('express').Router();
const BookModel = require('../models/bookModel')
const auth = require('../midleware/auth')

router.post('',auth,async(req,res) => {
    const book = new BookModel(req.body);
    try {
      await book.save();
      res.json(book);
    } catch (error) {
      res.status(500).send(error);
    }
})

module.exports = router;