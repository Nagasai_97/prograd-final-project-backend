const router = require('express').Router();
const RegisterModel = require('../models/authRegisterModel')
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
router.post("", async (req, res) => {

    let {firstname,lastname,email,password} =  req.body;
    let type = ''
    if(!email||typeof email !== 'string'){
      return res.status(500).json({status:'error',error:"Invalid email"})
    }
    if(!password ||typeof email !== 'string'){
      return res.status(500).json({status:'error',error:"Invalid password"})
    }
    const oldUser = await RegisterModel.findOne({ email });

    if (oldUser) {
      return res.status(500).json({status:500,data:"User Already Exist. Please Login"});
    }
    password = await bcrypt.hash(password,10)
    try {
      const user = await RegisterModel.create({
        firstname,
        lastname,
        email,
        password
      })
      if(email == process.env.EMAIL){
        type=process.env.ADMIN
      }
      else {
        type=process.env.USERROLE
      }
      const accesstoken = jwt.sign({
        id:user._id,
        username:user.email
      },process.env.SECRET_ACCESS_TOKEN,{expiresIn:'30s'})
      const refreshToken = jwt.sign({
        id:user._id,
        username:user.email
      },process.env.SECRET_REFRESH_TOKEN,{expiresIn:'7d'})
      res.cookie('accessToken',accesstoken)
      res.cookie('refreshToken',refreshToken)
      return res.json({status:200,accesstoken:accesstoken,refreshToken:refreshToken,type:type,id:user._id})
    } catch (error) {
      res.status(500).send(error);
    }
  });
  module.exports = router;