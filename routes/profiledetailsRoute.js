const router = require('express').Router();
const Register = require('../models/authRegisterModel')
const auth = require('../midleware/auth')

router.get('/:id',auth,async(req,res)=>{
    const id = req.params.id
    const profileDetails = await Register.findById(id);
    try{
      res.send(profileDetails)
    } catch(error) {
      res.status(500).send(error);
    }

})

module.exports = router;