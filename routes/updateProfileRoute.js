const router = require('express').Router();
const RegisterModel  = require('../models/authRegisterModel');
const auth = require('../midleware/auth')
const bcrypt = require('bcrypt');
router.put('/:id',auth,async(req,res)=>{
    const id = req.params.id
    let {firstname,lastname,email,password} =  req.body;
    password = await bcrypt.hash(password,10)
    let data = {firstname:firstname,lastname:lastname,email:email,password:password}
    const updateProfile = await RegisterModel.findByIdAndUpdate({_id:id},data,{new: true});
    try{
      res.send(updateProfile)
    } catch(error) {
      res.status(500).send(error);
    }

})

module.exports = router;