const router = require('express').Router();
const BookModel = require('../models/bookModel')

router.get('',async(req,res) => {
  console.log("hi")
    const books = await BookModel.find({}).where('status').equals('active');
    try {
      res.json(books);
    } catch (error) {
      res.status(500).send(error);
    }
})

module.exports = router;