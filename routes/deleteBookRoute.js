const router = require('express').Router();
const BookModel  = require('../models/bookModel');
const auth = require('../midleware/auth')

router.delete('/:id',auth,async(req,res)=>{
    const id = req.params.id
    const deleteBook = await BookModel.findByIdAndDelete({_id:id});
    try{
      res.send(deleteBook)
    } catch(error) {
      res.status(500).send(error);
    }

})

module.exports = router;