require('dotenv/config')
const router = require('express').Router();
const multer = require('multer')
const AWS = require('aws-sdk')
const { v4: uuidv4 } = require('uuid');
const s3 = new AWS.S3({
    region: 'us-east-2',
    accessKeyId: process.env.AWS_ID,
    secretAccessKey: process.env.AWS_SECRET
    
})


const storage = multer.memoryStorage({
    destination: function(req, file, callback) {
        callback(null, '')
    }
})

const upload = multer({storage}).single('pdf')
router.post('',upload,(req, res) => {
    const file = req.file;
    const fileType = file.fieldname
    const params = {
        Bucket: process.env.AWS_BUCKET_NAME,
        Key: `${uuidv4()}.${fileType}`,
        Body: req.file.buffer
    }
    s3.upload(params, (error, data) => {
        if(error){
            return res.status(500).send(error)
        }
        return res.status(200).send(data)
    })
})

module.exports = router;