const router = require('express').Router();
const BookModel = require('../models/bookModel')
const auth = require('../midleware/auth')
router.get('/:id',auth,async(req,res) => {
    const id = req.params.id
    const readBook = await BookModel.findById(id);
    try{
      res.send(readBook)
    } catch(error) {
      res.status(500).send(error);
    }
})

module.exports = router;