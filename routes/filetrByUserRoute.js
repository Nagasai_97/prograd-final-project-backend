const router = require('express').Router();
const BookModel = require('../models/bookModel')
const auth = require('../midleware/auth')
router.get('/:type/:id',auth,async(req,res) => {
    const type = req.params.type
    const id=req.params.id
    const myBooks = await BookModel.find({userid:id}).where('type').equals(type);
    try{
      res.send(myBooks)
    } catch(error) {
      res.status(500).send(error);
    }
})

module.exports = router;