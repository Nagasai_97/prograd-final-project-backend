const router = require('express').Router();
const LoginModel = require('../models/authRegisterModel'
)
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
require('dotenv').config();

router.post('',async(req,res) => {
    const {email,password} = req.body
    const user = await LoginModel.findOne({email}).lean()
    let type = ''
    if(!user){
      return res.status(500).json({status:400,error:"Invalid email/password"})
    }

    if(await bcrypt.compare(password,user.password)){
      if(email == process.env.EMAIL){
        type=process.env.ADMIN
      }
      else {
        type=process.env.USERROLE
      }
      const accesstoken = jwt.sign({
        id:user._id,
        username:user.email
      },process.env.SECRET_ACCESS_TOKEN,{expiresIn:'30s'})
      const refreshToken = jwt.sign({
        id:user._id,
        username:user.email
      },process.env.SECRET_REFRESH_TOKEN,{expiresIn:'7d'})
      res.cookie('accessToken',accesstoken)
      res.cookie('refreshToken',refreshToken)
      return res.json({status:200,accesstoken:accesstoken,refreshToken:refreshToken,type:type,id:user._id})
    }
    else {
      return res.status(500).json({status:400,error:"Invalid password"})
    }
  })


  module.exports = router;