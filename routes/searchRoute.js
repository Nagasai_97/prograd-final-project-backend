const router = require('express').Router();
const BookModel = require('../models/bookModel')
const auth = require('../midleware/auth')
router.get('/:search',auth,async(req,res) => {
    const search = req.params.search
    const searchBooks = await BookModel.find({bookname:{ $regex: search,$options: "$i" }});
    try{
      res.send(searchBooks)
    } catch(error) {
      res.status(500).send(error);
    }
})

module.exports = router;